#include<iostream>
#include<fstream>
#include<set>
#include<string>
#include<vector>
#include<map>
// Analysis -- The total time = number of map inserts and number of map lookups
//Add case flag
//std::set<std::string> compare_set;
std::map<std::string, int> compare_set;
std::map<std::string,int>::iterator it;
int main(int argc, char** argv){
  if(argc!=3){
    std::cout<<"Sample Usage ./compare <input> <output>"<<std::endl;
    return 0;
  }
  std::ifstream f1;
  std::ifstream f2;
  int count1(0),count2(0),count3(0);
  f1.open(argv[1], std::ios::in);
  f2.open(argv[2], std::ios::in);

  if(!f1.is_open()){
    std::cout<<"File 1 Error"<<std::endl;
  }
  if(!f2.is_open()){
    std::cout<<"File 2 Error"<<std::endl;
  }
  std::string temp1;
  while(!f1.eof()){
    f1>>temp1;
    if(!f1.eof()){
      count1+=1;
      compare_set.insert(make_pair(temp1,0));
    }
  }
  //  std::cout<<"File 1 Set Input complete"<<std::endl;
  std::string temp2;
  while(!f2.eof()){
    f2>>temp2;
    if(!f2.eof()){
      count2+=1;
      it = compare_set.find(temp2);
      if(it!=compare_set.end()){
	count3 +=1;
	it->second += 1;
      }
    }
  }
  int count4(0);
  for(it=compare_set.begin();it!=compare_set.end();++it){
    if(it->second == 0){
      //      std::cout<<it->first<<std::endl;
      count4+=1;
    }
  }

  //  std::cout<<"File 2 Set Input checked"<<std::endl;
  std::cout<<argv[1]<<" n(lines): "<<count1<<std::endl;
  std::cout<<argv[2]<<" n(lines): "<<count2<<std::endl;
  std::cout<<"Common n(lines): "<<count3<<std::endl;
  std::cout<<"Lines in 1 not in 2"<<count4<<std::endl;
  std::cout<<"Lines in 2 not in 1"<<count2-count3<<std::endl;
  
  return 0;
}
